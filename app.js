
/**
 * Module dependencies.
 */

//Mongoose dependency require 
require( './db' );

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var book = require('./routes/book');
var http = require('http');
var path = require('path');

var app = express();


// all environments
app.set('port', process.env.PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));

//Add to parse body of request
app.use(express.bodyParser());

//Add to use json objects
app.use(express.json());


var allowCrossDomain = function(req, res, next) {
res.header('Access-Control-Allow-Origin', '*');
res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
res.header('Access-Control-Allow-Headers', 'Content-Type, X-XSRF-TOKEN');
next();
}
app.use(allowCrossDomain);

app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}



app.get('/', routes.index);
app.get('/users', user.list);

//Configure URl paths for book
app.post('/books',book.create);
app.get('/books/:id', book.show);
app.get('/books',book.list);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
