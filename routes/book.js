
//Include DB variable
var mongoose = require("mongoose");

//Include Model variable
var Books = mongoose.model('Book');

//Object Id variable
var ObjectId = mongoose.Types.ObjectId;

//Url object
var url = require("url"); 
var http = require('http');


/*
 * GET Book listing.
 * Mapped to REST url pattern /books
 */

exports.list = function(request,response){

		Books.find(function(err,books,count){
			if(err)
			{
				response.writeHeader(404,{"Content-Type":"text/plain"});
				response.write("Object not Found");
				response.end();
			}
			else
			{

				var booksArray = [];

				books.forEach(function(book){
					var record = book.toObject();
  					record.location= "http://" + request.headers.host + url.parse(request.url).pathname +"/"+ book._id;
  					booksArray.push(record);
					});

				response.writeHeader(200,{"Content-Type":"application/json"});
				response.write(JSON.stringify(booksArray));
				response.end();	
			}
		})

	}



/*
 * POST Book listing.
 * Mapped to REST url pattern /books
 * Get fields author, name and description from POST and create object
 */


exports.create = function(request,response){

	var book = request.body;

	var new_book = new Books({
    	name    : book.name,
    	author : book.author,
    	description : book.description
  		});

	if(!(!!new_book.name && !!new_book.author && !!new_book.description))
	{
		response.writeHeader(400,{"Content-Type":"text/plain"});
				response.write("Invalid request. Check all fields");
				response.end();
	}
	else
	{


	new_book.save( function( err, books){
    		if(err)
			{
				response.writeHeader(503,{"Content-Type":"text/plain"});
				response.write("Object not saved");
				response.end();
			}
			else
			{
				response.writeHeader(201,{"Content-Type":"application/json","Location":"http://" + request.headers.host + url.parse(request.url).pathname +"/"+ books._id});
				response.write(JSON.stringify(books));
				response.end();	
			}
  			});
	}
}


exports.show = function(request,response){

	var id = request.params.id;
  	
	Books.findOne({"_id" : ObjectId(id)}, {author:1, name:1, description:1, _id:0} ,function(err,books,count){
			if(err || count==0)
			{
				response.writeHeader(404,{"Content-Type":"text/plain"});
				response.write("Object not Found");
				response.end();
			}
			else
			{
				response.writeHeader(200,{"Content-Type":"application/json"});
				response.write(JSON.stringify(books));
				response.end();	
			}
		})
}

