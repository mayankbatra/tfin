//Set the base url for rest calls
var rest_url_base = "http://ec2-54-254-216-249.ap-southeast-1.compute.amazonaws.com:8080"

$(document).ready(function() {

//Function to serialize an Object, for formatted json data
	$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};


//Set all alerts to use the alert close box and effects of bootstrap alert box
	$(".alert").alert();


/*
 * Function to get list of All Books from the rest API.
 * Tightly binded, should be separated into 2 functions, one for UI, the other for REST calls.
 * Called automtically when the page is loaded
 */

    $.ajax({
    	type: "GET",
        url: rest_url_base + "/books"
    }).then(function(data) {

    	$.each(data, function(i,item){

    		$(".book-data").append("<tr class='row-fluid single-book-data'  data-url= " +item.location + "><td>" + item._id +"</td><td>" + item.name + "</td><td>" + item.author + "</td><td>" + item.description + "</td></tr>");
    		
    	})
    });


/*
 * Function to get Details of a single Book from the rest API.
 * Tightly binded, should be separated into 2 functions, one for UI, the other for REST calls.
 * Called when user clicks on an item from the table of list of books
 */

    $('.book-data').on('click','.single-book-data',function(){
    	console.log($(this).attr('data-url'));
    	$.ajax({
    			type: "GET",
        		url: $(this).attr('data-url')
    		}).then(function(data) {

    		$("#alerts-container").append("<div class='toast toasttext01'>" + 
					 "<button type='button' class='close' data-dismiss='alert'></button>"+
					 "<div class='toast-body'>"+
					 "<h3 class='toast-heading'>" + data.name +"</h3>" +
					 "<small>" + data.author + "</small>" +
					 "<p>" + data.description+ "</p>"+
					 "</div>"+
					 "</div>");
    		
    });
    });



/*
 * Function to send form details via POST to the REST api, to create a new book.
 * Tightly binded, should be separated into 2 functions, one for UI, the other for REST calls.
 * Called when user Submits the form.
 * ToDo: Client Validation to be added.
 */


    $('#saveBook').on('click',function(e){
  /*      var sucess = true;
        $('input').each(function(){
          if(!$(this).val())
        {
          $(this).addClass('error');
          success = false;
        }

        });

        if(success)
        {
          */
        
          var formData = $("#book-create").serializeObject();
                  console.log(formData);
         $.ajax({
          type: "POST",             //Set request type PSOT
          url: rest_url_base + "/books/",   // Set URL for post request
          processData: "false", // Dont process data
          contentType:"application/json; charset=utf-8", // Set content type header
          dataType:"json", // Data Type Json
          data: JSON.stringify(formData), // Format json 
          
          // On success, show books details in an Alert box
          success: function(data){
            $("#alerts-container").append("<div class='toast toasttext01'>" + 
           "<button type='button' class='close' data-dismiss='alert'></button>"+
           "<div class='toast-body'>"+
           "<small> New Book Created </small>" +
           "<h3 class='toast-heading'>" + data.name +"</h3>" +
           "<small>" + data.author + "</small>" +
           "<p>" + data.description+ "</p>"+
           "</div>"+
           "</div>");

          
            $(".book-data").contents().remove();

            $.ajax({
                type: "GET",
                url: rest_url_base + "/books"
              }).then(function(data) {

              $.each(data, function(i,item){
                $(".book-data").append("<tr class='row-fluid single-book-data'  data-url= " +item.location + "><td>" + item._id +"</td><td>" + item.name + "</td><td>" + item.author + "</td><td>" + item.description + "</td></tr>");
        
              })
            });
          },
          // On Error, show Error details in an Alert box
          error: function(response){

            $("#alerts-container").append("<div class='toast toasttext01'>" + 
           "<button type='button' class='close' data-dismiss='alert'></button>"+
           "<div class='toast-body'>"+
           "<p>" + response.responseText+ "</p>"+
           "</div>"+
           "</div>");

          }

        });

    });

});